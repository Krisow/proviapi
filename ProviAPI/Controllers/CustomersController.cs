﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using LoanDbAccess;
using Microsoft.Extensions.Logging;

namespace WebAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/Customers")]
    public class CustomersController : Controller
    {
        private readonly LoansContext _context;
        private readonly ILogger<CustomersController> _logger;

        public CustomersController(LoansContext context, ILogger<CustomersController> logger)
        {
            _context = context;
            _logger = logger;
        }

        /// <summary>
        /// Returns list of 'CUSTOMER' objects 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetCustomers()
        {
            
            var cust = from a in _context.Customer select a;
            return Ok(cust);
        }

        /// <summary>
        /// Creates an 'CUSTOMER' object
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public IActionResult PostCustomer([FromBody] Models.CustomerRequest cust)
        {
            long temp;

            //Validation
            if(string.IsNullOrWhiteSpace(cust.Pin) || cust.Pin.Length != 11 || !long.TryParse(cust.Pin, out temp)) { return BadRequest("Invalid value in Pin field"); }
            if(string.IsNullOrWhiteSpace(cust.FirstName)) { return BadRequest("No value in FirstName field"); }
            if(string.IsNullOrWhiteSpace(cust.LastName)) { return BadRequest("No value in LastName field"); }
            if (_context.Customer.Any(o => o.Pin == cust.Pin) == true) { return BadRequest("Customer with given Pin already exists"); }
            try
            {
                Customer ncust = new Customer()
                {
                    Pin = cust.Pin,
                    FirstName = cust.FirstName,
                    LastName = cust.LastName,
                    Status = cust.Status,
                    CreateAtUtc = DateTime.UtcNow,
                    LastUpdatedAtUtc = DateTime.UtcNow
                };
                _context.Add(ncust);
                _context.SaveChanges();
                return Ok();
            }
            catch (Exception e)
            {
                _logger.LogError(e,e.Message);
                return BadRequest("Invalid request");
            }
        }

        /// <summary>
        /// Returns an 'CUSTOMER' object specified by given ID
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        [HttpGet("{customerId}")]
        public IActionResult GetCustomerById(int customerId)
        {
            var cust = (from a in _context.Customer where a.Id == customerId select a).FirstOrDefault();
            if (cust == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(cust);
            } 
        }
        /// <summary>
        /// Retrun an 'AGREEMENTS' objects for specific customer specified by given ID
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        [HttpGet("{customerId}/agreements")]
        public IActionResult GetAgreementsByCustomer(int customerId)
        {
            var agr = from a in _context.Agreement where a.CustomerId == customerId select a;
            return Ok(agr);
        }
    }

}