﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using LoanDbAccess;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace WebAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/Agreements")]
    public class AgreementsController : Controller
    {
        private readonly LoansContext _context;
        private readonly ILogger<AgreementsController> _logger;
        public AgreementsController(LoansContext context, ILogger<AgreementsController> logger)
        {
            _context = context;
            _logger = logger;
        }

        /// <summary>
        /// Returns list of 'AGREEMENT' objects 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetAgreements()
        {
            var agr = from a in _context.Agreement select a;
            return Ok(agr);
        }

        [HttpPost]
        public IActionResult PostAgreement([FromBody] Models.AgreementRequest agr)
        {
            //Validation
            if (_context.Customer.Any(o => o.Id == agr.CustomerId) == false) { return BadRequest("Customer with given id not found"); }
            if (_context.Agreement.Any(o => o.Number == agr.Number) == true) { return BadRequest("Agreement with given number already exists"); }

            try
            {
                Agreement nagr = new Agreement()
                {
                    Number = agr.Number,
                    Status = agr.Status,
                    CreateAtUtc = DateTime.UtcNow,
                    LastUpdatedAtUtc = DateTime.UtcNow,
                    IssuedAtUtc = agr.IssuedAtUtc,
                    RegularRate = agr.RegularRate,
                    CustomerId = agr.CustomerId
                };
                _context.Add(nagr);
                _context.SaveChanges();
                return Ok();
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
                return BadRequest("Invalid request");
            }
        }


        /// <summary>
        /// Returns list of 'AGREEMENT' objects specified by given ID
        /// </summary>
        /// <param name="agreementId"></param>
        /// <returns></returns>
        [HttpGet("{agreementId}")]
        public IActionResult GetAgreementById(int agreementId)
        {
            // JsonIgnore added to Customer entity. In other case there will be inf loop.
            var agr = (from a in _context.Agreement.Include("Customer")
                       where a.Id == agreementId 
                       select a).FirstOrDefault();

            if (agr == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(agr);
            }
        }


        // !!ATTENTION!!
        //Following method is same as previous - it will cause an error while requesting /agreement/{agreementId}
        //Intendent action?

        //[HttpGet("{agreementNumber}")]
        //public IActionResult GetAgreementById(int agreementNumber)
        //{
        //    // JsonIgnore added to Customer entity. In other case there will be inf loop.
        //    var agr = (from a in _context.Agreement.Include("Customer")
        //               where a.Id == agreementId
        //               select a).FirstOrDefault();

        //    if (agr == null)
        //    {
        //        return NotFound();
        //    }
        //    else
        //    {
        //        return Ok(agr);
        //    }
        //}



    }
}