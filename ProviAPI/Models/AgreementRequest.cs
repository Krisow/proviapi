﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Models
{
    public class AgreementRequest
    {
        public int Id { get; set; }
        public long? Number { get; set; }
        public string Status { get; set; }
        public DateTime? IssuedAtUtc { get; set; }
        public double? RegularRate { get; set; }
        public int? CustomerId { get; set; }

    }
}
