USE [Loans]
GO

ALTER TABLE [dbo].[Agreement] DROP CONSTRAINT [FK__Agreement__Custo__4CA06362]
GO

/****** Object:  Table [dbo].[Agreement]    Script Date: 22.11.2018 15:30:00 ******/
DROP TABLE [dbo].[Agreement]
GO

/****** Object:  Table [dbo].[Agreement]    Script Date: 22.11.2018 15:30:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Agreement](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Number] [bigint] NULL,
	[Status] [nvarchar](150) NULL,
	[CreateAtUtc] [date] NULL,
	[LastUpdatedAtUtc] [datetime] NULL,
	[IssuedAtUtc] [datetime] NULL,
	[RegularRate] [float] NULL,
	[CustomerId] [int] NULL,
 CONSTRAINT [PK_Agreement] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Agreement]  WITH CHECK ADD FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customer] ([Id])
GO


