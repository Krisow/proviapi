﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace LoanDbAccess
{
    public partial class LoansContext : DbContext
    {
        public LoansContext()
        {
        }

        public LoansContext(DbContextOptions<LoansContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Agreement> Agreement { get; set; }
        public virtual DbSet<Customer> Customer { get; set; }

        //Commented because of using DI 

//        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
//        {
//            if (!optionsBuilder.IsConfigured)
//            {
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
//                optionsBuilder.UseSqlServer("Server=KRZYSIEKDELLPC\\SQLEXPRESS;Database=Loans;Trusted_Connection=True;");
//            }
//        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Agreement>(entity =>
            {
                entity.Property(e => e.CreateAtUtc).HasColumnType("date");

                entity.Property(e => e.IssuedAtUtc).HasColumnType("datetime");

                entity.Property(e => e.LastUpdatedAtUtc).HasColumnType("datetime");

                entity.Property(e => e.Status).HasMaxLength(150);

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.Agreement)
                    .HasForeignKey(d => d.CustomerId)
                    .HasConstraintName("FK__Agreement__Custo__4CA06362");
            });

            modelBuilder.Entity<Customer>(entity =>
            {
                entity.Property(e => e.CreateAtUtc).HasColumnType("date");

                entity.Property(e => e.FirstName).HasMaxLength(250);

                entity.Property(e => e.LastName).HasMaxLength(250);

                entity.Property(e => e.LastUpdatedAtUtc).HasColumnType("datetime");

                entity.Property(e => e.Pin).HasMaxLength(50);

                entity.Property(e => e.Status).HasMaxLength(50);
            });
        }
    }
}
