﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace LoanDbAccess
{
    public partial class Agreement
    {
        public int Id { get; set; }
        public long? Number { get; set; }
        public string Status { get; set; }
        public DateTime? CreateAtUtc { get; set; }
        public DateTime? LastUpdatedAtUtc { get; set; }
        public DateTime? IssuedAtUtc { get; set; }
        public double? RegularRate { get; set; }
        public int? CustomerId { get; set; }
        [JsonIgnore]
        public Customer Customer { get; set; }

    }
}
