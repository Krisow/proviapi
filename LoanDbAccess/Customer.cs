﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace LoanDbAccess
{
    public partial class Customer
    {
        //Auto generated constructor
        public Customer()
        {
            Agreement = new HashSet<Agreement>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Pin { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Status { get; set; }
        public DateTime? CreateAtUtc { get; set; }
        public DateTime? LastUpdatedAtUtc { get; set; }
        [JsonIgnore]
        public ICollection<Agreement> Agreement { get; set; }
    }
}
